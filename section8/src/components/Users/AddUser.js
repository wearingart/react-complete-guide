import React, {useState, useRef} from "react";
import Button from "../UI/Button";
import Card from "../UI/Card";

import classes from './AddUser.module.css';
import ErrorModal from "../UI/ErrorModal";

const AddUser = (props) => {
	const nameInputRef = useRef();
	const ageInputRef = useRef();

	// const [enteredUsername, setEnteredUsername] = useState('');
	// const [enteredAge, setEnteredAge] = useState('');
	const [error, setError] = useState()

	const addUserHandler = (event) => {
		event.preventDefault();

		const enteredName = nameInputRef.current.value;
		const enteredUserAge = ageInputRef.current.value;

		if (enteredName.trim().length === 0 || enteredUserAge.trim().length === 0) {
			setError({
				title: 'Invalid input',
				message: "Please enter a valid name age (non-empty values)."
			});
			return;
		}
		if (+enteredUserAge < 1) {
			setError({
				title: 'Invalid age',
				message: "Please enter a valid age."
			});
			return;
		}

		props.onAddUser(enteredName, enteredUserAge);

		//Rarely use this. Ok to reset input
		nameInputRef.current.value = '';
		ageInputRef.current.value = '';

		// setEnteredUsername('');
		// setEnteredAge('');
	};

	// const usernameChangeHandler = (event) => {
	// 	setEnteredUsername(event.target.value.trim());
	// }
	//
	// const ageChangeHandler = (event) => {
	// 	setEnteredAge(event.target.value.trim());
	// }

	const errorHandler = () => {
		setError(null)
	}

	return (
		<React.Fragment>
			{error && <ErrorModal title={error.title} message={error.message} onConfirm={errorHandler} />}
			<Card className={classes.input}>
				<form onSubmit={addUserHandler}>
					<label htmlFor="username">Username</label>
					<input type="text"
						   id="username"
						   ref={nameInputRef}
					/>

					<label htmlFor="age">Age (Years)</label>
					<input type="number"
						   id="age"
						   ref={ageInputRef}
					/>

					<Button type="submit">Add User</Button>
				</form>
			</Card>
		</React.Fragment>
	)
};

export default AddUser;